<!DOCTYPE html>
<html>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<head>
    <title>Website Sample</title>
</head>
    <body>
    <center>
        <table border="1">
        <?php
        $player01 = array(
            "id" => "3",
            "name" => "梶谷隆幸",
            "position" => "外野手",
            "from" => "島根",
            "year" => "2007",
            );
        $player02 =  array(
            "id" => "44",
            "name" => "佐野恵太",
            "position" => "外野手",
            "from" => "岡山",
            "year" => "2017",
            );
        $player03 =  array(
            "id" => "15",
            "name" => "井納翔一",
            "position" => "投手",
            "from" => "東京",
            "year" => "2013",
            );

        $players = array($player01, $player02, $player03);

        foreach($players as $each){
            echo "<tr>";
            echo "<td>"."背番号: " . $each['id'] . "</td>"
                ."<td>"."名前: " . $each['name'] . "</td>"
                ."<td>"."ポジション: " . $each['position'] . "</td>"
                ."<td>"."出身地: " . $each['from'] . "</td>"
                ."<td>"."入団年: " . $each['year'] . "</td>";
            echo "</tr>";
}
        ?>
        </table>
    </center>
    </body>
    </html>
