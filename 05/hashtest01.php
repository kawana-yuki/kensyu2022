<!DOCTYPE html>
<html>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<head>
    <title>Website Sample</title>
</head>
    <body>
    <center>
        <?php
        $me_data = array(
            "fruit" => "スイカ",
            "sport" => "野球",
            "town" => "横浜",
            "age" => 21,
            "food" => "カレーライス"
        );

        $me_data = [
            "fruit2" => "すいか",
            "sport2" => "やきゅう",
            "town2" => "よこはま",
            "age2" => 22,
            "food2" => "かれーらいす",
        ];

        $me_data["fruit3"] = "西瓜";
        $me_data["sport3"] = "ヤキュウ";
        $me_data["town3"] = "ヨコハマ";
        $me_data["age3"] = "20";
        $me_data["food3"] = "🍛";
        var_dump($me_data);
        ?>
