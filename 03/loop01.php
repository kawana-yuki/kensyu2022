<!DOCTYPE html>
<html>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
    <head>
        <title>Website Sample</title>
    </head>
    <body>
        <center>
        <h1>ループ</h1>
        <form method='GET' action='loop01.php'>
            <table>
                <tr>
                    <th><input type="number" name="cols"></th>
                    <td>行のテーブルを出力</td>
                </tr>

                <tr>
                    <th><input type="reset" value="取消"></th>
                    <td><input type="submit" value="送信"></td>
                </tr>
            </table>
        </form>
        <table border="1">
            <?php
                for($i=0; $i < $_GET['cols']; $i++)
                {
                    if($i % 2 == 0)
                    {
                        echo "<tr bgcolor = #3fffff>
                                <td> $i </td>
                                <td> $i </td>
                                <td> $i </td>
                                <td> $i </td>
                                <td> $i </td>
                              </tr>";
                    }else{
                        echo "<tr bgcolor = #fffff2>
                                <td> $i </td>
                                <td> $i </td>
                                <td> $i </td>
                                <td> $i </td>
                                <td> $i </td>
                                </tr>";
                     }
                 }
            ?>
        </table>
        </center>
    </body>
</html>
